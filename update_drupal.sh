#!/bin/bash

function usage()
{
  echo "Usage: `basename \"$0\"` <site> [new.version]"
  echo "         - upgrades Drupal in subfolder <site> to [new.version]."
  echo "         - If [new.version] is ommitted, the script searches for a"\
                   "downloaded"
  echo "           tarball in the current directory."
  echo "       upgrade_drupal.sh [--h|--help]"
  echo "         - prints this message."
}

if [[ "$1" ]]
then
  if [[ "$1" = "-h" ]] || [[ "$1" = "--help" ]]
  then
    usage
  else
    if [[ "$2" ]]
    then
      archive="drupal-$2.tar.gz"
      if ! [[ -e $archive ]] # If the archive doesn't exist,
      then
        url="https://ftp.drupal.org/files/projects/$archive"
        echo "Downloading $archive…"
        wget $url # download it.
        echo "Download complete."
      fi
    else
      archives="drupal-*.tar.gz" # Set file mask to find downloaded Drupals
      let i=0 # Initiate the counter
      for a in $archives
      do
        if (($i==0))
        then
          archive=$a
        else
          echo "Multiple downloaded Drupal tarballs found."
          echo "Please leave only the tarball of the version you want to "\
               "upgrade to"
          echo "and remove the others. Then run this script again."
          echo "Alternatively, specify the version you want to upgrade to."
          exit
        fi
        # Update the counter
        i=$i+1 
      done
    fi
    
    # Now we have the downloaded archive name in the $archive variable.
    # Let's extract the contents.
    if [[ -e "$archive" ]]
    then
      echo "Unpacking archive $archive…"
      tar -zxf "$archive"
      
      drupaldir="${archive%.tar.gz}"
      echo Drupal directory: "$drupaldir"
      
      # Remove the default .htaccess file (the local .htaccess file 
      # may contain important changes essential for the site setup).
      echo "Excluding the original .htaccess file from installation"
      rm -vf "$drupaldir/.htaccess"
      echo "Exclusion complete."

      site="$1"
      # Get the original owner and group of the site folder.
      owner=`stat -c %u "$site"`:`stat -c %g "$site"`
      
      # Copy the rest of the files into the site folder
      # rewriting all target files.
      echo "Installing new Drupal files…"
      cp -R "$drupaldir"/* "$site"
      echo "Installation complete."
      
      # Revert the original owner and group of the site folder for all of its 
      # contents
      echo "Reverting original ownership of the site directory"
      echo "and all of its contents…"
      chown -R "$owner" "$site"
      echo "Reverted original ownership."
      
      # Cleanup
      echo "Cleaning up…"
      echo "  Removing temporary Drupal directory…"
      rm -Rf "$drupaldir"
      echo "  Removed temporary Drupal directory."
      echo "  Removing downloaded Drupal tarball…"
      rm -f "$archive"
      echo "  Removed temporary Drupal tarball."
      echo "Cleanup complete."
      
      echo "Done :-)"
    else
      echo "No version given and no downloaded tarball found."
      usage
      exit
    fi
  fi
else
  usage
fi
